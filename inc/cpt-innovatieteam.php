<?php 

 // Register Custom Post Type
function innovatieteam() {

	$labels = array(
		'name'                  => _x( 'Innovatieteam', 'Post Type General Name', 'Innovatieteam' ),
		'singular_name'         => _x( 'Innovatieteam', 'Post Type Singular Name', 'Innovatieteam' ),
		'menu_name'             => __( 'Innovatieteam', 'Innovatieteam' ),
		'name_admin_bar'        => __( 'Innovatieteam', 'Innovatieteam' ),
		'archives'              => __( 'Innovatieteam archief', 'Innovatieteam' ),
		'attributes'            => __( 'Item Attributes', 'Innovatieteam' ),
		'parent_item_colon'     => __( 'Hoofd teamlid', 'Innovatieteam' ),
		'all_items'             => __( 'Alle teamleden', 'Innovatieteam' ),
		'add_new_item'          => __( 'Nieuw teamlid toevoegen', 'Innovatieteam' ),
		'add_new'               => __( 'Nieuwe teamlid toevoegen', 'Innovatieteam' ),
		'new_item'              => __( 'Nieuwe teamlid', 'Innovatieteam' ),
		'edit_item'             => __( 'Teamlid bewerken', 'Innovatieteam' ),
		'update_item'           => __( 'Teamlid updaten', 'Innovatieteam' ),
		'view_item'             => __( 'Bekijk dit teamlid', 'Innovatieteam' ),
		'view_items'            => __( 'Bekijk teamleden', 'Innovatieteam' ),
		'search_items'          => __( 'Zoek een teamlid', 'Innovatieteam' ),
		'not_found'             => __( 'Teamlid niet gevonden', 'Innovatieteam' ),
		'not_found_in_trash'    => __( 'Teamlid niet gevonden in de prullenbak', 'Innovatieteam' ),
		'featured_image'        => __( 'Foto van de hoofdpersoon', 'Innovatieteam' ),
		'set_featured_image'    => __( 'Foto instellen', 'Innovatieteam' ),
		'remove_featured_image' => __( 'Foto verwijderen', 'Innovatieteam' ),
		'use_featured_image'    => __( 'Gebruik als hoofdafbeelding', 'Innovatieteam' ),
		'insert_into_item'      => __( 'Insert into item', 'Innovatieteam' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'Innovatieteam' ),
		'items_list'            => __( 'Items list', 'Innovatieteam' ),
		'items_list_navigation' => __( 'Items list navigation', 'Innovatieteam' ),
		'filter_items_list'     => __( 'Filter de teamleden', 'Innovatieteam' ),
	);
	$args = array(
		'label'                 => __( 'Innovatieteam', 'Innovatieteam' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions' ),
		'taxonomies'			=> array( '' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_in_rest' 			=> true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 23,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'innovatieteam',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'innovatieteam', $args );

}
add_action( 'init', 'innovatieteam', 0 );
