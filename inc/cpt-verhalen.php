<?php 

 // Register Custom Post Type
function verhalen() {

	$labels = array(
		'name'                  => _x( 'Verhalen', 'Post Type General Name', 'verhalen' ),
		'singular_name'         => _x( 'Verhaal', 'Post Type Singular Name', 'verhalen' ),
		'menu_name'             => __( 'Verhalen', 'verhalen' ),
		'name_admin_bar'        => __( 'Verhalen', 'verhalen' ),
		'archives'              => __( 'Verhalen archief', 'verhalen' ),
		'attributes'            => __( 'Item Attributes', 'verhalen' ),
		'parent_item_colon'     => __( 'Hoofdverhaal', 'verhalen' ),
		'all_items'             => __( 'Alle verhalen', 'verhalen' ),
		'add_new_item'          => __( 'Nieuw verhaal toevoegen', 'verhalen' ),
		'add_new'               => __( 'Nieuw verhaal toevoegen', 'verhalen' ),
		'new_item'              => __( 'Nieuw verhaal', 'verhalen' ),
		'edit_item'             => __( 'Verhaal bewerken', 'verhalen' ),
		'update_item'           => __( 'Verhaal updaten', 'verhalen' ),
		'view_item'             => __( 'Bekijk dit verhaal', 'verhalen' ),
		'view_items'            => __( 'Bekijk verhalen', 'verhalen' ),
		'search_items'          => __( 'Zoek een verhaal', 'verhalen' ),
		'not_found'             => __( 'Verhaal niet gevonden', 'verhalen' ),
		'not_found_in_trash'    => __( 'Verhaal niet gevonden in de prullenbak', 'verhalen' ),
		'featured_image'        => __( 'Foto van de hoofdpersoon', 'verhalen' ),
		'set_featured_image'    => __( 'Foto instellen', 'verhalen' ),
		'remove_featured_image' => __( 'Foto verwijderen', 'verhalen' ),
		'use_featured_image'    => __( 'Gebruik als hoofdafbeelding', 'verhalen' ),
		'insert_into_item'      => __( 'Insert into item', 'verhalen' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'verhalen' ),
		'items_list'            => __( 'Items list', 'verhalen' ),
		'items_list_navigation' => __( 'Items list navigation', 'verhalen' ),
		'filter_items_list'     => __( 'Filter de verhalen', 'verhalen' ),
	);
	$args = array(
		'label'                 => __( 'Verhaal', 'verhalen' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions' ),
		'taxonomies'			=> array( 'category' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_in_rest' 			=> true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-multisite',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'verhalen',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'verhalen', $args );

}
add_action( 'init', 'verhalen', 0 );



// function verhalen_taxonomy() {
// 	$labels = array(
// 	    'name' => _x( 'Groepen', 'taxonomy general name' ),
// 	    'singular_name' => _x( 'Groep', 'taxonomy singular name' ),
// 	    'search_items' =>  __( 'Doorzoek groepen' ),
// 	    'all_items' => __( 'Alle groepen' ),
// 	    'parent_item' => __( 'Hoofdgroep' ),
// 	    'parent_item_colon' => __( 'Hoofdgroep:' ),
// 	    'edit_item' => __( 'Group bewerken' ), 
// 	    'update_item' => __( 'Update groep' ),
// 	    'add_new_item' => __( 'Nieuw groep toevoegen' ),
// 	    'new_item_name' => __( 'Nieuwe groep naam' ),
// 	    'menu_name' => __( 'Groepen' ),
// 	);
// 	$args = array(
//         'labels'                     => $labels,
//         'hierarchical'               => true,
//         'public'                     => true,
//         'show_ui'                    => true,
//         'show_admin_column'          => true,
// 		'show_in_rest' 				 => true,
//         'show_in_nav_menus'          => true,
//         'show_tagcloud'              => true,
//     ); 	
//    	register_taxonomy( 'groepen', array( 'verhalen' ), $args );
// }
// add_action( 'init', 'verhalen_taxonomy', 0 );




/*-------------------------------------------------------------------------------
	Custom Columns Afbeeling toevoegen
-------------------------------------------------------------------------------*/

function my_page_columns($columns)
{
    $columns = array(
        'cb'         => '<input type="checkbox" />',
        'Overzichtsfoto'     => '',
        'title'     => 'Verhaal',
        'date'        =>    'Date',
    );
    return $columns;
}

function my_custom_columns($column)
{
    global $post;
    $verhaal_image = get_field('overzicht_afbeelding_verhaal');
    if( !empty( $verhaal_image ) ): 
    $verhaal_image_url = esc_url($verhaal_image['url']); ?>
	<?php endif; ?>

	<?php
    if ($column == 'Overzichtsfoto') {
        echo '<img src="' . $verhaal_image_url . '" width="100" />';
    }
    else {
         echo '';
    }
}

add_action("manage_verhalen_posts_custom_column", "my_custom_columns");
add_filter("manage_verhalen_posts_columns", "my_page_columns");


add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
   #Overzichtsfoto.column-Overzichtsfoto{width:110px;}
} 
</style>';
}